pipeline {
    agent any
    tools {
        nodejs "nodejs-10"
    }
   stages {
    stage('Pull') {
         steps {
            git branch: 'master',
                credentialsId: 'gascenzo',
                url: 'ssh://git@gitlab.com/gascenzo/who-am-i.git'  
         }
      }
    stage('Install') {
         steps {
            sh 'npm install -g @angular/cli'
            sh 'npm install'
         }
    }
    stage('Build') {
         steps {
            sh 'ng build'
            //sh 'ng build --prod'
         }
         post {
            success {
               sh 'ls -lia'
               //archiveArtifacts 'dist/raelix-angular-login'
            }
         }
      }
    stage('Delivery') {
        steps {
        sh 'echo "Not yet Implemented"'
        }
      }
    }
}